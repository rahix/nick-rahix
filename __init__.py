from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from ctx import Context
import leds

import json
import math


class App(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        try:
            with open("/flash/nick.json") as f:
                data = json.load(f)
        except OSError:
            data = {}

        self.name = data.get("name", "flow3r")

        self.time = 0
        self.led_rotation = 0
        self.text_cursor = 1
        self.rotation = 0

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        self.time += delta_ms / 1000
        self.led_rotation = math.floor(self.time * 5) % 40
        self.rotation = math.sin(self.time * math.pi) * math.pi * 0.1
        self.text_cursor = min(
            math.floor(self.time * 6) % (len(self.name) + 24) + 1,
            len(self.name),
        )

    def draw(self, ctx: Context) -> None:
        # Clear screen
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 1
        try:
            ctx.font = ctx.get_font_name(8)
        except ValueError:
            # Fallback if comic sans is missing
            ctx.font = ctx.get_font_name(5)

        text = self.name[: self.text_cursor]
        scale = 1 / ctx.text_width(text) * 220

        ctx.save()
        ctx.scale(scale, scale)
        ctx.rotate(self.rotation)
        ctx.rgb(0.36, 0.15, 0.53)
        ctx.move_to(0, 0)
        ctx.text(text)
        ctx.restore()

        color1 = 274
        color2 = 329
        for i in range(21):
            i20 = i / 20
            hue = i20 * color2 + (1 - i20) * color1
            value = i20 * 0.5 + (1 - i20) * 0.2
            leds.set_hsv((i + self.led_rotation) % 20, hue, 1, value)
            leds.set_hsv(39 - (i + self.led_rotation) % 20, hue, 1, value)
        leds.update()


if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(App(ApplicationContext()))
